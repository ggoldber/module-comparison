import tkinter as tk
from tkinter import filedialog
import os
import numpy as np
import matplotlib.pyplot as plt
from tkinter import messagebox
import re
from PIL import Image, ImageTk
import requests
from io import BytesIO

canvas = None
button_frame = None
background_image = None

#checking to see if the data has been processed yet
data_processed = False

#making background :)
def create_cute_border(app):
    global canvas, background_frame, button_frame,background_image,search_entry

    # Add a background image to the main window
    #rainbow logo
    background_url = "https://atlas.cern/sites/default/files/logo/ATLAS-Logo-LGBT-simple-transparent.png?_ga=2.199877486.1323062081.1689861216-2083147966.1689861216"
    #grey logo
    #background_url = "https://atlas.cern/sites/default/files/logo/ATLAS-Logo-BW-invert-RGB-H-trans.png?_ga=2.123758794.1323062081.1689861216-2083147966.1689861216"
    response = requests.get(background_url)
    background_image = ImageTk.PhotoImage(Image.open(BytesIO(response.content)))
    background_frame = tk.Label(app, image=background_image)
    background_frame.place(x=0, y=0, relwidth=1, relheight=1)

    # Create a canvas to hold the buttons and background image
    canvas = tk.Canvas(app, width=400, height=400, highlightthickness=0)
    canvas.place(relx=0, rely=0, relwidth=1, relheight=1)

    # Display the background image on the canvas
    canvas.create_image(0, 0, image=background_image, anchor="nw")

    # Create Buttons
    button1 = tk.Button(app, text="Select CERN folder", command=select_cern_folder, bg="SystemButtonFace")
    button2 = tk.Button(app, text="Select RAL folder", command=select_ral_folder, bg="SystemButtonFace")
    button3 = tk.Button(app, text="Process Files", command=open_files, bg="SystemButtonFace")
    search_button = tk.Button(app, text="Search for disagreement in data by channel number", command=perform_search,bg="SystemButtonFace")
    # Create the input field (text entry box) and search button
    search_entry = tk.Entry(app, width=30)

    # Display Buttons on the Canvas
    canvas.create_window(300, 50, window=button1)
    canvas.create_window(450, 50, window=button2)
    canvas.create_window(600, 50, window=button3)
    canvas.create_window(315, 85, window=search_button)
    canvas.create_window(630,85,window=search_entry)


#function to check for numbers
def is_float(value):
    pattern = r'^[-+]?(\d+(\.\d*)?|\.\d+)$'
    return re.match(pattern, value) is not None

#function to get info that is shown on noise plot
def get_noise_stats():
    global variance_noise
    global mean_diff_noise
    difference_in_noise = []  # Initialize the list outside the loop
    for key in disagreementn.keys():
        difference = abs(float(disagreementn[key][1]) - float(disagreementn[key][0]))
        difference_in_noise.append(difference)
    if len(difference_in_noise) > 0:
        variance_noise = int(np.var(difference_in_noise))
        mean_diff_noise = int(np.mean(difference_in_noise))
    else:
        variance_noise = 0
        mean_diff_noise = 0

#function to save .txt file 
def save_text():
    global txt_name
    try:
        file_txt = open("CERN module " + CERN_file_name + " vs" +" RAL module "+ RAL_file_name + ".txt","a")
        txt_name = "CERN module " + CERN_file_name + " vs" +" RAL module "+ RAL_file_name
    except:
        messagebox.showerror("Error", "Problem creating file.")
    else:
        file_txt.write(str(comparison_results_text))
        file_txt.close()

#function to save the plots
def save_plots():
    global filename_combined

    # Create the subplots
    fig, (ax1, ax2, ax3) = plt.subplots(3, figsize=(17, 15), sharex=True)

    # Plot for gain data
    ax1.scatter(channels_CERN, gain_CERN, c="red", alpha=.6, label="CERN" + " (mean = " + "{:.0f}".format(mean_gain_CERN)+")")
    ax1.scatter(channels_RAL, gain_RAL, c="blue", alpha=.6, label="RAL" + " (mean = " + "{:.0f}".format(mean_gain_RAL)+")")
    ax1.set_ylabel("Gain")
    ax1.set_title("Comparison of gain data module " + CERN_file_name)
    ax1.legend(loc=2)
    ax1.set_xticks([0, 500, 1000, 1500, 2000, 2500])

    # Plot for noise data
    ax2.scatter(channels_CERN, noise_CERN, c="red", alpha=.6, label="CERN" + " (mean = " + "{:.0f}".format(mean_noise_CERN) + "±" + "{:.0f}".format(variance_CERN) + ")")
    ax2.scatter(channels_RAL, noise_RAL, c="blue", alpha=.6, label="RAL" + " (mean = " + "{:.0f}".format(mean_noise_RAL) + "±" + "{:.0f}".format(variance_RAL) + ")")
    ax2.scatter(2559,800,c="white",alpha=.1,label = "mean difference in noise: " + str(mean_diff_noise) + " \n" + "variance in the difference in noise: " + str(variance_noise),s=1)
    ax2.set_ylabel("Noise")
    ax2.set_title("Comparison of noise data module " + CERN_file_name)
    #ax2.text(2000, 700, "mean difference in noise: " + str(mean_diff_noise) + " \n" + "variance in the difference in noise: " + str(variance_noise), fontsize=8)
    ax2.set_xticks([0, 500, 1000, 1500, 2000, 2500])
    ax2.legend(loc=2,prop={'size': 6})

    # Plot for vt50 data
    ax3.scatter(channels_CERN, vt50_CERN, c="red", alpha=.6, label="CERN" + " (mean = " + "{:.1f}".format(mean_vt50_CERN) + ")")
    ax3.scatter(channels_RAL, vt50_RAL, c="blue", alpha=.6, label="RAL" + " (mean = " + "{:.1f}".format(mean_vt50_RAL) + ")")
    ax3.set_xlabel("Channel")
    ax3.set_ylabel("vt50")
    ax3.set_title("Comparison of vt50 data module " + CERN_file_name)
    ax3.set_xticks([0, 500, 1000, 1500, 2000, 2500])
    ax3.legend(loc=2)

    #display plots
    plt.savefig("Comparison of data module " + CERN_file_name + ".png", dpi=100)
    plt.show()

    filename_combined = "Comparison of data module " + CERN_file_name + ".png"

#oh boy, open and process the files
def open_files():
    global CERN_data,channels_CERN,gain_CERN,noise_CERN,vt50_CERN,CERN_file_name,positive_channels_CERN,positive_gain_CERN,mean_gain_CERN,positive_channels_RAL,positive_gain_RAL,mean_gain_RAL,variance_RAL,variance_CERN,mean_vt50_RAL,mean_vt50_CERN,mean_noise_RAL,mean_noise_CERN,RAL_file_name,channels_RAL,gain_RAL,noise_RAL,vt50_RAL,folder1,files1,files2, folder2,data_processed
    if len(path1.get()) == 0 or len(path2.get()) == 0:
        messagebox.showerror("Error", "Please select both CERN and RAL folders.")
        return
    
    #assigning the selected folders to a variable name
    folder1 = path1.get()
    folder2 = path2.get()

    files1 = sorted(os.listdir(folder1))
    files2 = sorted(os.listdir(folder2))

    for file1, file2 in zip(files1, files2):
        with open(os.path.join(folder1, file1), 'r') as f1, open(os.path.join(folder2, file2), 'r') as f2:
            RAL_files = f1.read()
            CERN_files = f2.read()
            CERN_file = os.path.basename(file1)
            #indexing to the module #
            CERN_file_name = CERN_file[13:16]

            RAL_file = os.path.basename(file2)
            RAL_file_name = RAL_file[13:16]

            #splitting the file by each white space/linebreak
            list_of_info = RAL_files.split()
            cleaned_list_RAL = []
            #adding the data into a new list only if it is a number/decimal
            for i in list_of_info:
                if is_float(i):
                    cleaned_list_RAL += [float(i)]
                else:
                    continue
            
            #slicing list by indexing to specific info (ie. gain,noise,vt50)
            channels_RAL = cleaned_list_RAL[::5]
            gain_RAL = cleaned_list_RAL[2::5]
            noise_RAL = cleaned_list_RAL[4::5]
            vt50_RAL = cleaned_list_RAL[3::5]
            
            #splitting the file by each white space/linebreak
            list_of_info = CERN_files.split()
            cleaned_list_CERN = []
            #adding the data into a new list only if it is a number/decimal
            for i in list_of_info:
                if is_float(i):
                    cleaned_list_CERN += [float(i)]
                else:
                    continue
            
            #slicing list by indexing to specific info (ie. gain,noise,vt50)
            channels_CERN = cleaned_list_CERN[::5]
            gain_CERN = cleaned_list_CERN[2::5]
            noise_CERN = cleaned_list_CERN[4::5]
            vt50_CERN = cleaned_list_CERN[3::5]

            #taking only positive gain values for CERN and RAL
            positive_gain_CERN = []
            positive_channels_CERN = []

            for i in range(len(gain_CERN)):
                if float(gain_CERN[i]) > 0:
                    positive_gain_CERN.append(float(gain_CERN[i]))
                    positive_channels_CERN.append(float(channels_CERN[i]))
                    
            positive_gain_RAL = []
            positive_channels_RAL = []

            for i in range(len(gain_RAL)):
                if float(gain_RAL[i]) > 0:
                    positive_gain_RAL.append(float(gain_RAL[i]))
                    positive_channels_RAL.append(float(channels_RAL[i]))

            #calculating mean values for plots
            mean_gain_CERN = np.mean(positive_gain_CERN)
            mean_gain_RAL = np.mean(positive_gain_RAL)
            mean_noise_CERN = np.mean(noise_CERN)
            mean_noise_RAL = np.mean(noise_RAL)
            mean_vt50_CERN = np.mean(vt50_CERN)
            mean_vt50_RAL = np.mean(vt50_RAL)

            #calculating variance for noise
            variance_CERN = np.var(positive_gain_CERN)
            variance_RAL = np.var(positive_gain_RAL)

            global comparison_results_text
            global disagreementn
            comparison_results_text = ""
            #counting which points are the same and which are different for vt50
            counter1vt = 0
            counter2vt = 0
            disagreementvt = {}
            agreementvt = {}

            #checking for agreement/disagreement vt50
            for i in range(len(channels_CERN)):
              if vt50_CERN[i] != vt50_RAL[i]:
                #print(vt50_CERN[i],vt50_RAL[i])
                counter1vt += 1
                keyvt = int(channels_CERN[i])
                valuesvt = [float(vt50_CERN[i]),float(vt50_RAL[i])]
                disagreementvt["channel #"+str(keyvt)] = valuesvt
              else:
                counter2vt += 1
                keyvt = int(channels_CERN[i])
                valuesvt = [float(vt50_CERN[i]),float(vt50_RAL[i])]
                agreementvt["channel #"+str(keyvt)] = valuesvt

            #counting which points are the same and which are different for noise
            counter1n = 0
            counter2n = 0
            disagreementn = {}
            agreementn = {}

            #checking for agreement/disagreement in noise
            for i in range(len(channels_CERN)):
              if noise_CERN[i] != noise_RAL[i]:
                counter1n += 1
                keyn = int(channels_CERN[i])
                valuesn = [float(noise_CERN[i]),float(noise_RAL[i])]
                disagreementn["channel #"+str(keyn)] = valuesn
              else:
                counter2n += 1
                keyn = int(channels_CERN[i])
                valuesn = [float(noise_CERN[i]),float(noise_RAL[i])]
                agreementn["channel #"+str(keyn)] = valuesn
            

            #counting which points are the same and which are different for gain
            counter1g = 0
            counter2g = 0
            disagreementg = {}
            agreementg = {}
            
            #checking for agreement/disagreement gain
            for i in range(len(channels_CERN)):
              if gain_CERN[i] != gain_RAL[i]:
                counter1g += 1
                keyg = int(channels_CERN[i])
                valuesg = [float(gain_CERN[i]),float(gain_RAL[i])]
                disagreementg["channel #"+str(keyg)] = valuesg
              else:
                counter2g += 1
                keyg = int(channels_CERN[i])
                valuesg = [float(gain_CERN[i]),float(gain_RAL
                                                     [i])]
                agreementg["channel #"+str(keyg)] = valuesg
            #setting this to true so that the user can use the search by channel function later
            data_processed = True

            #calling functions for plots
            get_noise_stats()
            save_plots()

            # Store the comparison results text in the variable
            comparison_results_text = f"\n\nThere are {counter2g} channels that are the same and there are {counter1g} channels that are different from one another when comparing gain\n"
            comparison_results_text += f"\nThese are the channels that are the same {agreementg}\n"
            comparison_results_text += f"\nThese are the channels that are not the same {disagreementg}\n"
            comparison_results_text += f"\n\nThere are {counter2n} channels that are the same and there are {counter1n} channels that are different from one another when comparing noise.\n"
            comparison_results_text += f"\nThese are the channels that are the same {agreementn}\n"
            comparison_results_text += f"\nThese are the channels that are not the same {disagreementn}\n"
            comparison_results_text += f"\n\nThere are {counter2vt} channels that are the same and there are {counter1vt} channels that are different from one another when comparing vt50.\n"
            comparison_results_text += f"\nThese are the channels that are the same {agreementvt}\n"
            comparison_results_text += f"\nThese are the channels that are not the same {disagreementvt}\n"
            comparison_results_text += "\n\n"

            #saving text and letting the user know
            save_text()
            messagebox.showinfo("Plot Saved", f"Your plots have been saved as {filename_combined} \n \n Your text file has been saved as {txt_name}")

            result = messagebox.askyesno("Continue?", "Would you like to continue to the next file?")
            if not result:
                break

#function for user to search by channel #
def perform_search():

    #error messages for user
    if len(path1.get()) == 0 or len(path2.get()) == 0:
        messagebox.showerror("Error", "Please select both CERN and RAL folders.")
        return

    if not data_processed:
        messagebox.showerror("Data Not Processed", "You have to process the data files before searching the data.")
        return

    #getting just the number from their input
    search_query = search_entry.get()
    channel_num = ""
    for i in search_query:
        if i.isnumeric():
            channel_num += i
        else:
            continue

    if not channel_num.isdigit() or not (0 <= int(channel_num) <= 2560):
        tk.messagebox.showerror("Invalid Input", "Please input a channel number between 0 and 2560 (inclusive).")
        return
    
    disagreement_info_gain = "no disagreement"
    disagreement_info_noise = "no disagreement"
    disagreement_info_vt50 = "no disagreement"
        
    for file1, file2 in zip(files1, files2):
        with open(os.path.join(folder1, file1), 'r') as f1, open(os.path.join(folder2, file2), 'r') as f2:
            RAL_files = f1.read()
            CERN_files = f2.read()
            CERN_file = os.path.basename(file1)
            CERN_file_name = CERN_file[13:16]

            RAL_file = os.path.basename(file2)
            RAL_file_name = RAL_file[13:16]

            #splitting the file by each white space/linebreak
            list_of_info = RAL_files.split()
            cleaned_list_RAL = []
            #adding the data into a new list only if it is a number/decimal
            for i in list_of_info:
                if is_float(i):
                    cleaned_list_RAL += [float(i)]
                else:
                    continue
            
            #slicing list by indexing to specific info (ie. gain,noise,vt50)
            channels_RAL = cleaned_list_RAL[::5]
            gain_RAL = cleaned_list_RAL[2::5]
            noise_RAL = cleaned_list_RAL[4::5]
            vt50_RAL = cleaned_list_RAL[3::5]
            
            #splitting the file by each white space/linebreak
            list_of_info = CERN_files.split()
            cleaned_list_CERN = []
            #adding the data into a new list only if it is a number/decimal
            for i in list_of_info:
                if is_float(i):
                    cleaned_list_CERN += [float(i)]
                else:
                    continue
            
            #slicing list by indexing to specific info (ie. gain,noise,vt50)
            channels_CERN = cleaned_list_CERN[::5]
            gain_CERN = cleaned_list_CERN[2::5]
            noise_CERN = cleaned_list_CERN[4::5]
            vt50_CERN = cleaned_list_CERN[3::5]

            #taking only positive gain values for CERN and RAL
            positive_gain_CERN = []
            positive_channels_CERN = []

            for i in range(len(gain_CERN)):
                if float(gain_CERN[i]) > 0:
                    positive_gain_CERN.append(float(gain_CERN[i]))
                    positive_channels_CERN.append(float(channels_CERN[i]))
                    
            positive_gain_RAL = []
            positive_channels_RAL = []

            for i in range(len(gain_RAL)):
                if float(gain_RAL[i]) > 0:
                    positive_gain_RAL.append(float(gain_RAL[i]))
                    positive_channels_RAL.append(float(channels_RAL[i]))

            #calculating mean values for plots
            mean_gain_CERN = np.mean(positive_gain_CERN)
            mean_gain_RAL = np.mean(positive_gain_RAL)
            mean_noise_CERN = np.mean(noise_CERN)
            mean_noise_RAL = np.mean(noise_RAL)
            mean_vt50_CERN = np.mean(vt50_CERN)
            mean_vt50_RAL = np.mean(vt50_RAL)

            #calculating variance for noise
            variance_CERN = np.var(positive_gain_CERN)
            variance_RAL = np.var(positive_gain_RAL)

            global comparison_results_text
            global disagreementn
            comparison_results_text = ""
            #counting which points are the same and which are different for vt50
            counter1vt = 0
            counter2vt = 0
            disagreementvt = {}
            agreementvt = {}

            #checking for agreement/disagreement vt50
            for i in range(len(channels_CERN)):
              if vt50_CERN[i] != vt50_RAL[i]:
                #print(vt50_CERN[i],vt50_RAL[i])
                counter1vt += 1
                keyvt = int(channels_CERN[i])
                valuesvt = [float(vt50_CERN[i]),float(vt50_RAL[i])]
                disagreementvt[str(keyvt)] = valuesvt
              else:
                counter2vt += 1
                keyvt = int(channels_CERN[i])
                valuesvt = [float(vt50_CERN[i]),float(vt50_RAL[i])]
                agreementvt[str(keyvt)] = valuesvt

            #counting which points are the same and which are different for noise
            counter1n = 0
            counter2n = 0
            disagreementn = {}
            agreementn = {}

            #checking for agreement/disagreement in noise
            for i in range(len(channels_CERN)):
              if noise_CERN[i] != noise_RAL[i]:
                counter1n += 1
                keyn = int(channels_CERN[i])
                valuesn = [float(noise_CERN[i]),float(noise_RAL[i])]
                disagreementn[str(keyn)] = valuesn
              else:
                counter2n += 1
                keyn = int(channels_CERN[i])
                valuesn = [float(noise_CERN[i]),float(noise_RAL[i])]
                agreementn[str(keyn)] = valuesn
            

            #counting which points are the same and which are different for gain
            counter1g = 0
            counter2g = 0
            disagreementg = {}
            agreementg = {}
            
            #checking for agreement/disagreement gain
            for i in range(len(channels_CERN)):
              if gain_CERN[i] != gain_RAL[i]:
                counter1g += 1
                keyg = int(channels_CERN[i])
                valuesg = [float(gain_CERN[i]),float(gain_RAL[i])]
                disagreementg[str(keyg)] = valuesg
              else:
                counter2g += 1
                keyg = int(channels_CERN[i])
                valuesg = [float(gain_CERN[i]),float(gain_RAL[i])]
                agreementg[str(keyg)] = valuesg

        #finding the channel info for display message
        for key in disagreementg.keys():
            if int(key) == int(channel_num):
                disagreement_info_gain = disagreementg[key]

        for key in disagreementn.keys():
            if int(key) == int(channel_num):
                disagreement_info_noise = disagreementn[key]

        for key in disagreementvt.keys():
            if int(key) == int(channel_num):
                disagreement_info_vt50 = disagreementvt[key]

        message = f"For channel {channel_num} in {CERN_file_name}, the values in noise are {disagreement_info_noise}, the values in gain are {disagreement_info_gain}, the values in vt50 are {disagreement_info_vt50}"

        #display the search in a messagebox
        tk.messagebox.showinfo(f"Search Result: ", message)
                    

#folder selection
def select_cern_folder():
    folder_path = filedialog.askdirectory()
    path1.set(folder_path)
    tk.messagebox.showinfo("Success","CERN folder has been selected")

def select_ral_folder():
    folder_path = filedialog.askdirectory()
    path2.set(folder_path)
    tk.messagebox.showinfo("Success","RAL folder has been selected")

# Create the main GUI window
app = tk.Tk()
app.title("Module Comparison Interface")
app.geometry("900x400")

path1 = tk.StringVar()
path2 = tk.StringVar()


# Call the function to create the cute border
create_cute_border(app)

app.mainloop()
